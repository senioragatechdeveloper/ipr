import Vuex from 'vuex'
import { ad } from '@/types/index'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      ads: [
        {
          id: 0,
          title: 'title',
          price: 1200,
          date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
          description: 'description'
        },
        {
          id: 1,
          title: 'title1',
          price: 13200,
          date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
          description: 'description'
        },
        {
          id: 2,
          title: 'title2',
          price: 1400,
          date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
          description: 'description'
        },
        {
          id: 3,
          title: 'title3',
          price: 1100,
          date: (new Date(Date.now() - (new Date()).getTimezoneOffset() * 60000)).toISOString().substr(0, 10),
          description: 'description'
        }
      ],
      user: {
        name: 'User',
        money: 1500,
        authorized: false
      }
    }),
    mutations: {
      addAds (state, payload: ad): void {
        state.ads.push(payload)
      },
      removeAd (state, id) {
        state.ads = state.ads.filter(item => item.id !== id)
      },
      saveAd (state, payload) {
        state.ads = state.ads.map((item) => {
          if (item.id === payload.id) {
            item = {
              ...payload
            }
          }
          return item
        })
      },
      editUser (state, payload) {
        state.user = payload
      },
      updateMoney (state, money) {
        state.user.money = money
      }
    }
  })
}

export default createStore
