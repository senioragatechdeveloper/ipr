export type ad = {
  id: number;
  title: string;
  price: number;
  date: string;
  description: string;
};

export type user = {
  name:string,
  money: number,
  authorized: boolean
};
